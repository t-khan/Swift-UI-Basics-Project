//
//  Swift_UI_Basics_ProjectApp.swift
//  Swift UI Basics Project
//
//  Created by Tofik Khan on 4/27/21.
//

import SwiftUI

@main
struct Swift_UI_Basics_ProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
