//
//  DetailView.swift
//  Swift UI Basics Project
//
//  Created by Tofik Khan on 4/27/21.
//

import SwiftUI

struct DetailView: View {
    
    @State var popOverIsShowing = false
    
    var data: TableData
    
    var body: some View {
        VStack {
            Text(data.detail)
            Button("Show Popover") {
                popOverIsShowing = true;
            }
            .padding()
            .sheet(isPresented: $popOverIsShowing, content: {
                Text(data.title)
                    .padding()
                    .font(.largeTitle)
                Text(data.detail)
                    .padding()
                    .font(.title)
            })
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(data: DataModel.data[0])
    }
}
